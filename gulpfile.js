const gulp = require('gulp');
const ts = require('gulp-typescript');
const tsconfig = require('./tsconfig.json');
var tsProject = ts.createProject(tsconfig.compilerOptions);

function compile() {
  return gulp.src('src/**/*.ts')
    .pipe(tsProject())
    .js
      .pipe(gulp.dest('dist'));
}

function watch() {
  gulp.watch('src/**/*.ts', compile);
}

gulp.task('compile', compile);
gulp.task('default', compile);
gulp.task('watch', watch);