import { toPoints } from 'svg-points';

interface Point {
  x: number;
  y: number;
  moveTo?: boolean;
}

export default async function pathToPoints(arrayOfPaths):Promise<Array<Array<Point>>> {
  let points:Array<Array<Point>> = new Array();
  arrayOfPaths.forEach((val, key)=>{
    val.$.type = 'path';
    points.push(toPoints(val.$));
  });
  return points;
}