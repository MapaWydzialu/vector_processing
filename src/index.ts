import * as program from "commander";
import {isUndefined} from "lodash";
import readXml from "./readFile";
import selectLayer from "./selectLayer";
import pathToPoints from './pathToPoints';
import saveToSQL from './saveToSQL';
program
  .version('1.0.0')
  .usage('[options] <file ...>')
  .option('-f, --file <file>', 'process pointed vector file')
  .option('-d, --directory <path>', 'process all files in pointed directory')
  .option('-l, --layer <layerName>', 'select proper layer name')
  .parse(process.argv);

if(program.file) {
  readXml(program.file)
    .then(response=>selectLayer(response, !isUndefined(program.layer)?program.layer:'kontur'))
    .then(response=>pathToPoints(response.path))
    .then(response=>{
      console.log(response);
    })
    .catch(reason=>{
      console.log(`No such file like: ${program.file}`);
    });
}
else if(program.directory) {
  console.log(`Process directory /${program.directory}`);
} else {
  console.log(`Select file or directory.`);
}