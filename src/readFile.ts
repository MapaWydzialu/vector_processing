import * as fs from "fs";
import {parseString} from "xml2js";

export default function readXML(path) {
  return new Promise<string>((resolve, reject)=>{
    fs.readFile(path, (err, data) => {
      if (err) 
        return reject(err);
      let xml = data.toString('UTF-8');
      parseString(xml, (err, result)=>{
        resolve(result.svg.g);
      });
    });
  });
}