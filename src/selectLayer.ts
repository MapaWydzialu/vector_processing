import * as _ from "lodash";

export default function selectLayer(jsonSVG, layerName) {
  let selectedLayerObject = null;
  selectedLayerObject = _.find(jsonSVG,function(o){
    return o.$['inkscape:label'] === layerName;
  });
  return selectedLayerObject;
}